window.addEventListener('load', function() {
	ymaps.ready(initMap);
});

function initMap() {
	var myMap = new ymaps.Map(
		"map", {
			center: [45.098493, 39.011003],
			zoom: 15,
			height: 520,
			controls: []
		}
	);
	myMap.behaviors.disable('scrollZoom');

	var dataMarkers = [{
			position: [45.098493, 39.011003],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder--main.svg',
			iconImageSize: [63, 63],
			text: 'КП "Зеленая роща"',
			type: 'main',
			zIndex: 2,
		},
		{
			position: [45.099450, 39.012751],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Лидер',
			type: 'school',
			zIndex: 1,
		},
		{
			position: [45.111949, 39.016304],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'МБОУ Нош № 94',
			type: 'school',
			zIndex: 1,
		},
		{
			position: [45.120431, 39.025425],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Специализированная психиатрическая больница № 7',
			type: 'hospital',
			zIndex: 1,
		},
		{
			position: [45.117650, 39.026634],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Психиатрическая больница',
			type: 'hospital',
			zIndex: 1,
		},
		{
			position: [45.114260, 39.016953],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Продукты',
			type: 'shop',
			zIndex: 1,
		},
		{
			position: [45.095267, 39.016598],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Фрукты овощи',
			type: 'shop',
			zIndex: 1,
		},
		{
			position: [45.099805, 39.013343],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Остановка',
			type: 'bus',
			zIndex: 1,
		},
		{
			position: [45.104700, 39.012936],
			iconLayout: 'default#image',
			iconImageHref: 'images/placeholder.svg',
			iconImageSize: [38, 56],
			text: 'Остановка',
			type: 'bus',
			zIndex: 1,
		},
	];


	var markers = [];

	if (windowWidth < 768) {
		for (var i = 0; i < dataMarkers.length; i++) {
			dataMarkers[i].iconImageSize = [70, 70];
		}
	}

	addMarkers(myMap, dataMarkers);


	function addMarkers(map, arr) {
		var marker;
		for (var i = 0; i < arr.length; i++) {
			marker = new ymaps.Placemark(
				arr[i].position, {
					hintContent: arr[i].text,
					balloonContent: arr[i].type == 'main' ? '<div class="balloonMain">'+arr[i].text+'</div>' : '<div class="balloonDefault">'+arr[i].text+'</div>',
					type: arr[i].type
				}, {
					card: arr[i].card,
					iconLayout: 'default#image',
					iconImageHref: arr[i].iconImageHref,
					iconImageSize: arr[i].iconImageSize, // размер иконки
					iconImageOffset: arr[i].iconImageOffset !== undefined ? arr[i].iconImageOffset : [-arr[i].iconImageSize[0] / 2, -arr[i].iconImageSize[1]],
					balloonLayout: "default#imageWithContent", 
					balloonContentSize: [260, 100],
					balloonImageOffset: [-130, -120],
					zIndex: arr[i].zIndex,
					hideIconOnBalloonOpen: false,
				}
			);
			marker.events.add('click', function(e){
				var a = e.get('target').geometry.getCoordinates();
				myMap.panTo(a, {
					flying: 1
				});
			});
			markers.push(marker);
			myMap.geoObjects.add(marker);
		}
	}

	myMap.events.add('balloonopen', function(e) {
		var balloon = e.get('balloon');
		myMap.events.add('click', function(e) {
			if (e.get('target') === myMap) { 
				myMap.balloon.close();
				var a = e.get('target').geometry.getCoordinates();
				myMap.panTo(a, {
					flying: 1
				});
			}
		});
	});

	function filterBy(param) {
		var marker;
		for (var i = 0; i < markers.length; i++) {
			var marker = markers[i];
			if(marker.properties.get('type') == param || marker.properties.get('type') == 'main' || param == 'all') {
				marker.options.set('visible', true);
			} else {
				marker.options.set('visible', false);
			}
		}
		myMap.setBounds(myMap.geoObjects.getBounds(), {
			checkZoomRange: true,
			zoomMargin: 20
		});
	}

	if(windowWidth < 769) {
		filterBy('main');
		myMap.setBounds(myMap.geoObjects.getBounds(), {
			checkZoomRange: true,
			zoomMargin: 80
		});
	}

	document.querySelectorAll('.b-location__filter').forEach(function(element) {
		element.addEventListener('click', function() {
			var data = this.getAttribute('data-filter');
			filterBy(data);
			var filters = this.parentNode.children;
			Array.prototype.forEach.call(filters, function(element) {
				element.classList.remove('-active');
			});
			this.classList.add('-active');
		});
	});
}