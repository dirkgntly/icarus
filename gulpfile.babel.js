import gulp from 'gulp'
import sourcemaps from 'gulp-sourcemaps'
import cleanCSS from 'gulp-clean-css'
import del from 'del'
import browserSync from 'browser-sync'
import rollup from 'gulp-better-rollup'
import babel from 'rollup-plugin-babel'
import eslint from 'gulp-eslint'

import postcss from 'gulp-postcss'
import postImport from 'postcss-import'
import postNested from 'postcss-nested'
import postcf from 'postcss-color-function'
import postna from 'postcss-nested-ancestors'
import postcm from 'postcss-custom-media'
import postav from 'postcss-advanced-variables'
import postMixins from 'postcss-sassy-mixins'
import postCalc from 'postcss-calc'
import postDublicates from 'postcss-discard-duplicates'
import postCSSNano from 'cssnano'
import autoprefixer from 'autoprefixer'
import lost from 'lost'
import csso from 'gulp-csso'

import plumber from 'gulp-plumber'
import gulpPug from 'gulp-pug'
import typograf from 'gulp-typograf'
import injectSvg from 'gulp-inject-svg'

const paths = {
  pug: {
    src: 'src/pug/pages/*.pug',
    dest: 'dist/'
  },
  styles: {
    src: 'src/styles/**/*.css',
    dest: 'dist/styles/css',
    root: 'src/styles/*.css',
    maps: 'maps'
  },
  scripts: {
    src: 'src/scripts/**/*.js',
    dest: 'dist/scripts/'
  },
  files: {
    src: 'src/files/*/*.*',
    dest: 'dist/files'
  },
  fonts: {
    src: 'src/fonts/*/*.*',
    dest: 'dist/fonts/'
  }
}

const clean = () => del(['dist'])

const server = browserSync.create()

export function reload (done) {
  server.reload()
  done()
}

export function serve (done) {
  server.init({
    server: {
      baseDir: './dist',
      notify: true,
      open: false,
      ghostMode: true
    }
  })
  done()
}

export function styles () {
  const plugins = [
    postImport(),
    postMixins(),
    postcm(),
    postav(),
    postna({
      levelSymbol: '*'
    }),
    postNested(),
    postcf(),
    postCalc({
      mediaQueries: true
    }),
    lost(),
    postDublicates(),
    postCSSNano({
      discardComments: {
        removeAll: true
      }
    }),
    autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    })
  ]
  return gulp.src(paths.styles.root)
    .pipe(sourcemaps.init())
    .pipe(
      postcss(plugins)
    )
    .pipe(csso())
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe(sourcemaps.write(paths.styles.maps))
    .pipe(gulp.dest(paths.styles.dest))
};

export function pug () {
  return gulp.src(paths.pug.src)
    .pipe(plumber())
    .pipe(gulpPug({
      pretty: true
    }))
    .pipe(typograf({
      locale: ['ru', 'en-US'],
      disableRule: ['ru/other/phone-number']
    }))
    .pipe(injectSvg({
      base: '/src'
    }))
    .pipe(gulp.dest(paths.pug.dest))
}

export function scripts () {
  return gulp.src(paths.scripts.src)
    .pipe(eslint({
      configFile: './.eslintrc.yml'
    }))
    .pipe(eslint.format())
    .pipe(rollup({
      treeshake: false,
      plugins: [
        babel()
      ]
    }, {
      format: 'es'
    }))
    .pipe(gulp.dest(paths.scripts.dest))
}

function watchFiles () {
  gulp.watch(paths.pug.src, pug)
  gulp.watch(paths.styles.src, styles)
  gulp.watch(paths.scripts.src, scripts)
}

const dev = gulp.series(clean, styles, pug, scripts, watchFiles, serve)
const watch = gulp.parallel(watchFiles, serve)

exports.watch = watch
exports.default = dev
